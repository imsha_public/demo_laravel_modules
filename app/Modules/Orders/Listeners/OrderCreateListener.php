<?php

namespace App\Modules\Orders\Listeners;

use App\Modules\Invoices\Services\InvoiceServiceInterface;
use App\Modules\Orders\Events\OrderCreateEvent;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderCreateListener implements ShouldQueue
{
    protected InvoiceServiceInterface $invoiceService;

    public function __construct(InvoiceServiceInterface $invoiceService)
    {
        $this->invoiceService = $invoiceService;
    }

    public function handle(OrderCreateEvent $event): void
    {
        $this->invoiceService->generate($event->getOrder());
    }
}

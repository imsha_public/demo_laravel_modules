<?php

namespace App\Modules\Orders\Events;

use App\Dto\OrderDto;
use Illuminate\Queue\SerializesModels;

class OrderCreateEvent
{
    use SerializesModels;

    protected OrderDto $order;

    public function __construct(OrderDto $order)
    {
        $this->order = $order;
    }

    /**
     * @return OrderDto
     */
    public function getOrder(): OrderDto
    {
        return $this->order;
    }
}

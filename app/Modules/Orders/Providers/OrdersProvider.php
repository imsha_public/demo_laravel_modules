<?php

namespace App\Modules\Orders\Providers;

use App\Modules\Orders\Events\OrderCreateEvent;
use App\Modules\Orders\Listeners\OrderCreateListener;
use Illuminate\Events\Dispatcher;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class OrdersProvider extends ServiceProvider
{
    protected function registerCommands(): void
    {
        $this->commands([
            //....
        ]);
    }

    protected function bindListeners()
    {
        /** @var Dispatcher $events */
        $events = $this->app->make('events');

        $events->listen(OrderCreateEvent::class, [OrderCreateListener::class, 'handle']);
    }

    protected function registerServices(): void
    {
        //... регистрация сервисов
    }

    public function register()
    {
        $this->registerServices();
    }

    public function boot()
    {
        $this->registerCommands();
        $this->bindListeners();
    }
}

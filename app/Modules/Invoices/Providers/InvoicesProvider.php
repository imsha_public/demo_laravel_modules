<?php

namespace App\Modules\Invoices\Providers;

use App\Modules\Invoices\Services\InvoiceService;
use App\Modules\Invoices\Services\InvoiceServiceInterface;
use App\Modules\Orders\Events\OrderCreateEvent;
use App\Modules\Invoices\Listeners\OrderCreateListener;
use Illuminate\Events\Dispatcher;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class InvoicesProvider extends ServiceProvider
{
    protected function registerCommands(): void
    {
        $this->commands([
            //....
        ]);
    }

    protected function bindListeners()
    {
        /** @var Dispatcher $events */
        $events = $this->app->make('events');

        $events->listen(OrderCreateEvent::class, [OrderCreateListener::class, 'handle']);
    }

    protected function registerServices(): void
    {
        $this->app->singleton(InvoiceServiceInterface::class, InvoiceService::class);
    }

    public function register()
    {
        $this->registerServices();
    }

    public function boot()
    {
        $this->registerCommands();
        $this->bindListeners();
    }
}

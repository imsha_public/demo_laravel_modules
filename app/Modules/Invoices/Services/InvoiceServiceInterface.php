<?php

namespace App\Modules\Invoices\Services;

use App\Dto\OrderDto;

interface InvoiceServiceInterface
{
    /**
     * @param OrderDto $order
     * @return void
     */
    public function generate(OrderDto $order) : void;
}
